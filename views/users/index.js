'use strict'

exports.inicio = function (req, res) {
  req.app.db.models.user.find({}, function (err, users) {
    res.render('users/hola', {
      'titulo': 'CRUD clinica',
      users
    })
  })
}

exports.ingresar = function (req, res) {
  const item = req.body
  console.log(item)
  req.app.db.models.user.create(item, function (err, data) {
    if (err) {
      console.log('fallo al ingresar! ' + err)
      res.send(['ERROR'])
    } else {
      console.log('Se ingreso correctamente!')
      res.send(['OK'])
    }
  })
}
exports.actualizar = function (req, res) {
  console.log('Actualizar')
}
exports.eliminar = function (req, res) {
  console.log('Eliminar')
}
