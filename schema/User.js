'use strict'

exports = module.exports = function (app, mongoose) {
  var userSchema = new mongoose.Schema({
    username: { type: String, unique: true },
    password: String,
    email: { type: String, unique: true },
    isActive: String
  })
  userSchema.set('autoIndex', (app.get('env') === 'development'))
  app.db.model('user', userSchema)
}
